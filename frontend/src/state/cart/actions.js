import { 
  ADD_TO_CART,
  INCREASE_CART_QUANTITY,
  DECREASE_CART_QUANTITY,
  REMOVE_FROM_CART
} from "./types"

export const addToCart = product => {
  return {
    type: ADD_TO_CART,
    payload: {
      product
    }
  }
}

export const removeFromCart = product => {
    return {
      type: REMOVE_FROM_CART,
      payload: {
        product
      }
    }
}

export const increaseCartQuantity = product => {
  return {
    type: INCREASE_CART_QUANTITY,
    payload: {
      product
    }
  }
}

export const decreaseCartQuantity = product => {
  return {
    type: DECREASE_CART_QUANTITY,
    payload: {
      product
    }
  }
}