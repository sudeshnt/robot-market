import { 
  ADD_TO_CART,
  REMOVE_FROM_CART,
  INCREASE_CART_QUANTITY,
  DECREASE_CART_QUANTITY
} from './types';

const initialState = {
  items: [],
  totalItems: 0,
  total: 0
}

const cartReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch(type) {
    case ADD_TO_CART:
      return {
        ...state,
        items: [
          ...state.items,
          payload.product
        ]
      };
    case REMOVE_FROM_CART:
      return {
        ...state,
        items: state.items?.filter(item => item.id !== payload.product.id)
      };
    case INCREASE_CART_QUANTITY:
      return {
        ...state,
        items: state.items?.map(item => 
          item.id === payload.product.id ? {
            ...item,
            qty: item.qty + 1
          } : item
        )
      };
    case DECREASE_CART_QUANTITY:
      return {
        ...state,
        items: state.items?.map(item => 
          item.id === payload.product.id ? {
            ...item,
            qty: item.qty - 1
          } : item
        )
      };
    default:
      return state;
  }
}

export default cartReducer;