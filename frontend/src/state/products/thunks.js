import { message } from 'antd';
import { 
  fetchProductsInProgress,
  fetchProductsSuccess,
  fetchProductsFailure,
  fetchMaterialsSuccess,
} from "./actions";
import { addToCart, increaseCartQuantity } from '../cart/actions';
import * as productsApi from "../../api/products";
import { MAXIMUM_CART_ITEMS_ALLOWED } from '../../config/config';

export const fetchProducts = _ => async dispatch => {
  try {
    dispatch(fetchProductsInProgress());
    const response = await productsApi.fetchProducts();
    const products = response?.data?.data ?? [];
    const materials = [
      ...new Set(products.map(product => product.material))
    ];
    dispatch(fetchProductsSuccess(products))
    dispatch(fetchMaterialsSuccess(materials))
  } catch(e) {
    dispatch(fetchProductsFailure(e))
  }
}

export const addProductToCart = (product, cartItems) => dispatch => {
  const productAlreadyInCart = cartItems.some(item => item.id === product.id);
  if (productAlreadyInCart) {
    dispatch(increaseCartQuantity(product))
  } else {
    if (cartItems.length === MAXIMUM_CART_ITEMS_ALLOWED) {
      message.error("Sorry.. can't add more than 5 robots to cart");
    } else {
      product = {
        ...product,
        qty: 1
      };
      dispatch(addToCart(product))
    }
  }
}
