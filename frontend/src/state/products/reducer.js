import {
  ADD_TO_CART,
  DECREASE_CART_QUANTITY,
  INCREASE_CART_QUANTITY,
  REMOVE_FROM_CART } from '../cart/types';
import {
  FETCH_PRODUCTS_IN_PROGRESS,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_PRODUCTS_ERROR,
  FETCH_MATERIALS_SUCCESS,
} from './types'

const initialState = {
  items: [],
  materials: [],
  loading: false,
  error: null
}

const ProductReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch(type) {
    case FETCH_PRODUCTS_IN_PROGRESS:
      return {
        ...state,
        loading: true,
        error: null
      };
    case FETCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        loading: false,
        items: payload
      }
    case FETCH_PRODUCTS_ERROR:
      return {
        ...state,
        loading: false,
        error: payload
      }
    case FETCH_MATERIALS_SUCCESS:
      return {
        ...state,
        materials: payload,
      }
    case ADD_TO_CART:
    case INCREASE_CART_QUANTITY:
      return {
        ...state,
        items: state.items?.map(item => 
          item.id === payload.product.id ? {
            ...item,
            stock: item.stock - 1
          } : item
        )
      }
    case DECREASE_CART_QUANTITY:
      return {
        ...state,
        items: state.items?.map(item => 
          item.id === payload.product.id ? {
            ...item,
            stock: item.stock + 1
          } : item
        )
      }
    case REMOVE_FROM_CART:
      return {
        ...state,
        items: state.items?.map(item => 
          item.id === payload.product.id ? {
            ...item,
            stock: item.stock + payload.product.qty
          } : item
        )
      }
    default:
      return state;
  }
}

export default ProductReducer;