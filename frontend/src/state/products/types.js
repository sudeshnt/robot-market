export const FETCH_PRODUCTS_IN_PROGRESS = "FETCH_PRODUCTS_IN_PROGRESS";
export const FETCH_PRODUCTS_SUCCESS = "FETCH_PRODUCTS_SUCCESS";
export const FETCH_PRODUCTS_ERROR = "FETCH_PRODUCTS_ERROR";
export const FETCH_MATERIALS_SUCCESS = "FETCH_MATERIALS_SUCCESS";