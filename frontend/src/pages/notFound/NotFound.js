import { Empty } from 'antd';
import styled from 'styled-components';

const NotFound = () => {

  return (
    <Styled.Container>
      <Styled.PageNotFound />
    </Styled.Container>
  )
}

const Styled = {
  Container: styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
  `,
  PageNotFound: styled(Empty).attrs(() => ({
    description: 'Page Not Found'
  }))`
  `
}

export default NotFound;