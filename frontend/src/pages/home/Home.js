import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Row, Col } from "antd";
import ProductGrid from "../../components/products/productGrid/ProductGrid";
import Cart from "../../components/cart/cart/Cart";
import { fetchProducts } from "../../state/products/thunks";

const Home = () => {
  const dispatch = useDispatch();
  const { items, loading, materials } = useSelector(state => state.products);

  useEffect(() => {
    dispatch(fetchProducts())
  }, []);

  return(
    <div>
        <div className="left">
          <ProductGrid 
            products={items}
            loading={loading}
            materials={materials} />
        </div>
        <div className="right">
          <Cart />
        </div>
    </div>
  )
}

export default Home;