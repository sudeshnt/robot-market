import * as React from 'react';
import { Layout, Typography } from 'antd';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import styled from 'styled-components';
import Home from './pages/home/Home';
import NotFound from './pages/notFound/NotFound';
import './App.less';

function App() {
  
  return (
    <BrowserRouter>
      <div className="App">
        <Styled.Layout>
          <Styled.Header>
            <div>
              <Styled.Title level={3}>
                Robot Market
              </Styled.Title>
            </div>
          </Styled.Header>
          <Styled.Content>
            <Switch>
              <Route exact path="/" component={Home}/>
              <Route component={NotFound}/>
            </Switch>
          </Styled.Content>
        </Styled.Layout>
      </div>
    </BrowserRouter>
  );
}

const Styled = {
  Layout: styled(Layout)`
    &.ant-layout {
      min-height: 100vh;
    }
  `,
  Header: styled(Layout.Header)`
    display: flex;
  `,
  Content: styled(Layout.Content)`
    height: 100%;
  `,
  Title: styled(Typography.Title)`
    display: flex;
    width: 200px;
    margin-top: 1rem;
    color: white !important;
  `,
}

export default App;
