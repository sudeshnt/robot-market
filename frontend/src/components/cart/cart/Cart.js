import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Descriptions, Divider, Empty, Typography } from "antd";
import styled from "styled-components";
import { formatToTHB } from "../../../utils/currency";
import CartItem from "../cartItem/CartItem";

const Cart = () => {
  const cartItems = useSelector(state => state.cart.items);
  const [ total, setTotal ] = useState(0)

  const { Container, Title } = Styled;

  useEffect(() => {
    const total = cartItems.reduce((acc, item) => {
      return acc + item.qty * item.price
    }, 0)
    setTotal(total)
  }, [cartItems])

  return (
    <Container>
      <Title>My Cart</Title>
      <Descriptions
        layout="horizontal"
        column={1}
        bordered
        size='small'
        contentStyle={{ background: 'white', fontSize: '1rem', fontWeight: 500}}
        labelStyle={{ fontSize: '1rem', fontWeight: 500 }}
      >
        <Descriptions.Item label="Total Amount">{cartItems.length}</Descriptions.Item>
        <Descriptions.Item label="Total Price">{formatToTHB(total)}</Descriptions.Item>
      </Descriptions>
      <Divider />
      { 
        cartItems.length ?
          cartItems.map(item => (
            <CartItem key={item.id} item={item} />
          )) : <Empty
          image='https://www.hydroplus.co.in/images/empty_cart.png'
          imageStyle={{ height: 150 }}
          description=""/>
      }
      <Divider />
    </Container>
  )
}

const Styled = {
  Container: styled.div`
    padding-right: 1rem
  `,
  Title: styled(Typography.Title).attrs(() => ({
    level: 3
  }))`
    color: #001628;
    margin-top: 2rem;
    margin-bottom: 0;
  `
}

export default Cart;