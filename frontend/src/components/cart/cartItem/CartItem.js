import { useDispatch } from "react-redux";
import { Card, Row, Col, Image, Typography, Button } from "antd"
import {
  PlusOutlined,
  MinusOutlined,
  DeleteOutlined
} from '@ant-design/icons';
import PropTypes from 'prop-types';
import styled from "styled-components"
import { formatToTHB } from "../../../utils/currency";
import { decreaseCartQuantity, increaseCartQuantity, removeFromCart } from "../../../state/cart/actions";

const CartItem = ({ item }) => {

  const dispatch = useDispatch()
  const { Title } = Typography;
  const { Container, ItemDetailsContainer, Quantity } = Styled;

  const onDecreaseQty = item => {
    dispatch(decreaseCartQuantity(item))
  }

  const onIncreaseQty = item => {
    dispatch(increaseCartQuantity(item))
  }

  const onRemoveItem = (item) => {
    dispatch(removeFromCart(item))
  }

  return (
    <Container hoverable>
      <Row>
        <Col>
          <Image 
            preview={false}
            width={85}
            height={'100%'}
            fallback="https://www.bkgymswim.com.au/wp-content/uploads/2017/08/image_large.png"
            src={item.image}
            placeholder={
              <Image
                preview={false}
                width={'100%'}
                height={85}
                src="https://upload.wikimedia.org/wikipedia/commons/2/29/Loader.gif"
              />
            } />
        </Col>
        <ItemDetailsContainer flex={1}>
          <Row>
            <Col flex={2}>
              <Title level={5}>{item.name}</Title>
            </Col>
            <Col flex={3}>
              <Title className="price" level={5}>{formatToTHB(item.price)}</Title>
            </Col>
          </Row>
          <Row style={{marginTop: '8px'}} >
              <Col flex={1}>
                <Button
                  danger
                  shape="circle"
                  icon={<DeleteOutlined />}
                  onClick={() => onRemoveItem(item)} />
              </Col>
              <Col flex={1}>
                <Row justify="end">
                  <Button 
                    shape="circle"
                    icon={<MinusOutlined /> }
                    disabled={item.qty === 1}
                    onClick={() => onDecreaseQty(item)}/>
                  <Quantity level={5}>{item.qty}</Quantity>
                  <Button 
                    shape="circle"
                    icon={<PlusOutlined />}
                    disabled={item.qty === item.stock}
                    onClick={() => onIncreaseQty(item)}/>
                </Row>
              </Col>
          </Row>
        </ItemDetailsContainer>
      </Row>
    </Container>
  )
}

const Styled = {
  Container: styled(Card)`
    margin: 0.5rem 0;

    .ant-card-body {
      padding: 0;
    }
  `,
  ItemDetailsContainer: styled(Col)`
    padding: 0.5rem;
    padding-right: 1rem;
  `,
  Quantity: styled(Typography.Title).attrs(() => ({
    level: 5
  }))`
    margin: 0.2rem 1rem;
  `
}

CartItem.propTypes = {
  item: PropTypes.object
}

export default CartItem;