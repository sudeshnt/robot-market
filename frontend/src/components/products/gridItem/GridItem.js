import { useDispatch, useSelector } from "react-redux";
import { Button, Card, Image, Row, Col, Typography, Badge, Space } from "antd";
import { ShoppingCartOutlined } from '@ant-design/icons';
import styled from "styled-components";
import moment from "moment"
import PropTypes from 'prop-types'; 
import { addProductToCart } from "../../../state/products/thunks";
import { formatToTHB } from "../../../utils/currency";

const GridItem = ({ product }) => {
  const cartItems = useSelector(state => state.cart.items)
  const dispatch = useDispatch();

  const { Card, CardMetaData, AddToCartButton, BottomRow, CreatedText, Space } = Styled;
  const { Title, Text } = Typography;

  const addToCart = (product) => {
    dispatch(addProductToCart(product, cartItems))
  }

  return (
    <Card>
      <Row style={{ minHeight: 145}}>
        <Image
          src={product.image}
          width='100%'
          fallback="https://www.bkgymswim.com.au/wp-content/uploads/2017/08/image_large.png"
          placeholder={
            <Image
              preview={false}
              width={'100%'}
              height={160}
              src="https://upload.wikimedia.org/wikipedia/commons/2/29/Loader.gif"
            />
          }/>
      </Row>
      <Row>
        <CardMetaData>
          <Row>
            <AddToCartButton disabled={!product.stock} onClick={() => addToCart(product)}/>
          </Row>
          <Row>
            <Space direction="vertical">
              <Title className="title" level={5}>{product.name}</Title>
              <Badge className="material-badge">{product.material}</Badge>
              <CreatedText>created on {moment(product.createdAt).format('DD-MM-YYYY')}</CreatedText>
            </Space>
          </Row>
          <BottomRow>
            <Col flex={2}>
              {
                product.stock ? <div>
                  <Text className="stock-text">Stock: </Text> {product.stock}
                </div> : <Text type="danger">out of stock</Text>
              }
            </Col>
            <Col flex={3}>
              <Title className="price" level={5}>{
                formatToTHB(product.price)
              }</Title>
            </Col>
          </BottomRow>
        </CardMetaData>
      </Row>
    </Card>
  )
}

const Styled = {
  Card: styled(Card).attrs(() => ({
    hoverable: true,
  }))`
    min-height: 250px;
    min-width: 150px;

    & .ant-card-body {
      padding: 0;
    }
  `,
  AddToCartButton: styled(Button).attrs(() => ({
    type: 'primary',
    shape: 'circle',
    icon: <ShoppingCartOutlined />,
    size: 'large'
  }))`
    margin: -20px 0 0 auto;
  `,
  CardMetaData: styled.div`
    flex: 1;
    padding: 0.2rem 0.5rem ;
  `,
  BottomRow: styled(Row)`
    flex: 1;
    height: 40px;
    align-items: flex-end;
  `,
  PriceText: styled(Typography.Title).attrs(() => ({
    level: 5
  })),
  CreatedText: styled(Typography.Text)`
    font-size: 0.7rem;
    color: gray;
  `,
  MaterialText: styled(Badge)`
    position: absolute;
  `,
  Space: styled(Space)`
    & .ant-space-item {
      margin: 0 !important;
    }
  `
}

GridItem.propTypes = {
  product: PropTypes.object
}

export default GridItem;