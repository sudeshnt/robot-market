import { useState } from "react";
import { Row, Col, Select, Typography } from "antd";
import PropTypes from 'prop-types'; 
import styled from "styled-components";
import GridItem from "../gridItem/GridItem";

const Styled = {
  Row: styled(Row)`
    padding: 0.5rem 1rem;
    display: flex;
    justify-content: space-between;
  `,
  Col: styled(Col)`
    margin: 0.5rem
  `,
  FilterRow: styled(Row)`
    padding: 1rem 1.5rem;
    display: flex;
    justify-content: flex-end;
  `,
  FilterLabel: styled(Typography.Title).attrs(() => ({
    level: 5
  }))`
    margin: 3px 8px;
  `,
  Filter: styled(Select).attrs(() => ({
    placeholder: "select material",
    style: { width: 150 },
    allowClear: true
  }))`
  
  `
}

const ProductGrid = ({ products, materials, loading }) => {
  const { Row, Col, FilterRow, FilterLabel, Filter } = Styled
  const [ filter, setFilter ] = useState(null)

  const onFilterChange = (value) => {
    setFilter(value);
  }

  return(
    <div>
      <FilterRow>
        <FilterLabel>Filter by Material:</FilterLabel>
        <Filter onChange={onFilterChange} loading={loading} >
          {
            materials?.map(material => (
              <Select.Option key={material} value={material}>{material}</Select.Option>
            ))
          }
        </Filter>
      </FilterRow>
      <div className="product-grid">
        <Row>
          {
            products?.map(product => {
              return filter && product.material !== filter ? null : (
                <Col key={product.id} sm={12} md={8} lg={6}>
                  <GridItem product={product} />
                </Col>
              )
            })
          }
        </Row>
      </div>
    </div>
  )
}

ProductGrid.propTypes = {
  products: PropTypes.array,
  materials: PropTypes.array,
  loading: PropTypes.bool
}

export default ProductGrid;